// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.20.1
// source: yumontime/pointservice/points.proto

package pointsservicepb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// PointServiceClient is the client API for PointService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PointServiceClient interface {
	// GetPointsHistory can be used to access all kinds of points history
	GetPointsHistory(ctx context.Context, in *GetPointsHistoryRequest, opts ...grpc.CallOption) (*GetPointsHistoryReply, error)
	// AddPoints can be used to add points for all kinds of activities
	AddPoints(ctx context.Context, in *AddPointsRequest, opts ...grpc.CallOption) (*AddPointsReply, error)
	GetTotalPoints(ctx context.Context, in *GetTotalPointsRequest, opts ...grpc.CallOption) (*GetTotalPointsReply, error)
	GetTotalExperience(ctx context.Context, in *GetTotalExperienceRequest, opts ...grpc.CallOption) (*GetTotalExperienceReply, error)
}

type pointServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPointServiceClient(cc grpc.ClientConnInterface) PointServiceClient {
	return &pointServiceClient{cc}
}

func (c *pointServiceClient) GetPointsHistory(ctx context.Context, in *GetPointsHistoryRequest, opts ...grpc.CallOption) (*GetPointsHistoryReply, error) {
	out := new(GetPointsHistoryReply)
	err := c.cc.Invoke(ctx, "/PointService/GetPointsHistory", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *pointServiceClient) AddPoints(ctx context.Context, in *AddPointsRequest, opts ...grpc.CallOption) (*AddPointsReply, error) {
	out := new(AddPointsReply)
	err := c.cc.Invoke(ctx, "/PointService/AddPoints", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *pointServiceClient) GetTotalPoints(ctx context.Context, in *GetTotalPointsRequest, opts ...grpc.CallOption) (*GetTotalPointsReply, error) {
	out := new(GetTotalPointsReply)
	err := c.cc.Invoke(ctx, "/PointService/GetTotalPoints", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *pointServiceClient) GetTotalExperience(ctx context.Context, in *GetTotalExperienceRequest, opts ...grpc.CallOption) (*GetTotalExperienceReply, error) {
	out := new(GetTotalExperienceReply)
	err := c.cc.Invoke(ctx, "/PointService/GetTotalExperience", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PointServiceServer is the server API for PointService service.
// All implementations must embed UnimplementedPointServiceServer
// for forward compatibility
type PointServiceServer interface {
	// GetPointsHistory can be used to access all kinds of points history
	GetPointsHistory(context.Context, *GetPointsHistoryRequest) (*GetPointsHistoryReply, error)
	// AddPoints can be used to add points for all kinds of activities
	AddPoints(context.Context, *AddPointsRequest) (*AddPointsReply, error)
	GetTotalPoints(context.Context, *GetTotalPointsRequest) (*GetTotalPointsReply, error)
	GetTotalExperience(context.Context, *GetTotalExperienceRequest) (*GetTotalExperienceReply, error)
	mustEmbedUnimplementedPointServiceServer()
}

// UnimplementedPointServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPointServiceServer struct {
}

func (UnimplementedPointServiceServer) GetPointsHistory(context.Context, *GetPointsHistoryRequest) (*GetPointsHistoryReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPointsHistory not implemented")
}
func (UnimplementedPointServiceServer) AddPoints(context.Context, *AddPointsRequest) (*AddPointsReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddPoints not implemented")
}
func (UnimplementedPointServiceServer) GetTotalPoints(context.Context, *GetTotalPointsRequest) (*GetTotalPointsReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTotalPoints not implemented")
}
func (UnimplementedPointServiceServer) GetTotalExperience(context.Context, *GetTotalExperienceRequest) (*GetTotalExperienceReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTotalExperience not implemented")
}
func (UnimplementedPointServiceServer) mustEmbedUnimplementedPointServiceServer() {}

// UnsafePointServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PointServiceServer will
// result in compilation errors.
type UnsafePointServiceServer interface {
	mustEmbedUnimplementedPointServiceServer()
}

func RegisterPointServiceServer(s grpc.ServiceRegistrar, srv PointServiceServer) {
	s.RegisterService(&PointService_ServiceDesc, srv)
}

func _PointService_GetPointsHistory_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetPointsHistoryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PointServiceServer).GetPointsHistory(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/PointService/GetPointsHistory",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PointServiceServer).GetPointsHistory(ctx, req.(*GetPointsHistoryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PointService_AddPoints_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddPointsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PointServiceServer).AddPoints(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/PointService/AddPoints",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PointServiceServer).AddPoints(ctx, req.(*AddPointsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PointService_GetTotalPoints_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetTotalPointsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PointServiceServer).GetTotalPoints(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/PointService/GetTotalPoints",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PointServiceServer).GetTotalPoints(ctx, req.(*GetTotalPointsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PointService_GetTotalExperience_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetTotalExperienceRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PointServiceServer).GetTotalExperience(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/PointService/GetTotalExperience",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PointServiceServer).GetTotalExperience(ctx, req.(*GetTotalExperienceRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// PointService_ServiceDesc is the grpc.ServiceDesc for PointService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var PointService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "PointService",
	HandlerType: (*PointServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetPointsHistory",
			Handler:    _PointService_GetPointsHistory_Handler,
		},
		{
			MethodName: "AddPoints",
			Handler:    _PointService_AddPoints_Handler,
		},
		{
			MethodName: "GetTotalPoints",
			Handler:    _PointService_GetTotalPoints_Handler,
		},
		{
			MethodName: "GetTotalExperience",
			Handler:    _PointService_GetTotalExperience_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "yumontime/pointservice/points.proto",
}
