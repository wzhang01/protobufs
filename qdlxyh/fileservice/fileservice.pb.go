// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.17.3
// source: qdlxyh/fileservice/fileservice.proto

package fileservicepb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type SearchFileRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
}

func (x *SearchFileRequest) Reset() {
	*x = SearchFileRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchFileRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchFileRequest) ProtoMessage() {}

func (x *SearchFileRequest) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchFileRequest.ProtoReflect.Descriptor instead.
func (*SearchFileRequest) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{0}
}

func (x *SearchFileRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

type SearchFileReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status int64   `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	File   []*File `protobuf:"bytes,2,rep,name=file,proto3" json:"file,omitempty"`
}

func (x *SearchFileReply) Reset() {
	*x = SearchFileReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchFileReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchFileReply) ProtoMessage() {}

func (x *SearchFileReply) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchFileReply.ProtoReflect.Descriptor instead.
func (*SearchFileReply) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{1}
}

func (x *SearchFileReply) GetStatus() int64 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *SearchFileReply) GetFile() []*File {
	if x != nil {
		return x.File
	}
	return nil
}

type GetFileRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetFileRequest) Reset() {
	*x = GetFileRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetFileRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetFileRequest) ProtoMessage() {}

func (x *GetFileRequest) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetFileRequest.ProtoReflect.Descriptor instead.
func (*GetFileRequest) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{2}
}

func (x *GetFileRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetFileReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status int64 `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	File   *File `protobuf:"bytes,2,opt,name=file,proto3" json:"file,omitempty"`
}

func (x *GetFileReply) Reset() {
	*x = GetFileReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetFileReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetFileReply) ProtoMessage() {}

func (x *GetFileReply) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetFileReply.ProtoReflect.Descriptor instead.
func (*GetFileReply) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{3}
}

func (x *GetFileReply) GetStatus() int64 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *GetFileReply) GetFile() *File {
	if x != nil {
		return x.File
	}
	return nil
}

type GetAllFilesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page int64 `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
}

func (x *GetAllFilesRequest) Reset() {
	*x = GetAllFilesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllFilesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllFilesRequest) ProtoMessage() {}

func (x *GetAllFilesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllFilesRequest.ProtoReflect.Descriptor instead.
func (*GetAllFilesRequest) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{4}
}

func (x *GetAllFilesRequest) GetPage() int64 {
	if x != nil {
		return x.Page
	}
	return 0
}

type GetAllFilesReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status    int64   `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	Files     []*File `protobuf:"bytes,2,rep,name=files,proto3" json:"files,omitempty"`
	Page      int64   `protobuf:"varint,3,opt,name=page,proto3" json:"page,omitempty"`
	TotalPage int64   `protobuf:"varint,4,opt,name=totalPage,proto3" json:"totalPage,omitempty"`
}

func (x *GetAllFilesReply) Reset() {
	*x = GetAllFilesReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllFilesReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllFilesReply) ProtoMessage() {}

func (x *GetAllFilesReply) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllFilesReply.ProtoReflect.Descriptor instead.
func (*GetAllFilesReply) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{5}
}

func (x *GetAllFilesReply) GetStatus() int64 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *GetAllFilesReply) GetFiles() []*File {
	if x != nil {
		return x.Files
	}
	return nil
}

func (x *GetAllFilesReply) GetPage() int64 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetAllFilesReply) GetTotalPage() int64 {
	if x != nil {
		return x.TotalPage
	}
	return 0
}

type GetMyFilesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uid string `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
}

func (x *GetMyFilesRequest) Reset() {
	*x = GetMyFilesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetMyFilesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetMyFilesRequest) ProtoMessage() {}

func (x *GetMyFilesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetMyFilesRequest.ProtoReflect.Descriptor instead.
func (*GetMyFilesRequest) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{6}
}

func (x *GetMyFilesRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

type GetMyFilesReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status int64   `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	Files  []*File `protobuf:"bytes,2,rep,name=files,proto3" json:"files,omitempty"`
}

func (x *GetMyFilesReply) Reset() {
	*x = GetMyFilesReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetMyFilesReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetMyFilesReply) ProtoMessage() {}

func (x *GetMyFilesReply) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetMyFilesReply.ProtoReflect.Descriptor instead.
func (*GetMyFilesReply) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{7}
}

func (x *GetMyFilesReply) GetStatus() int64 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *GetMyFilesReply) GetFiles() []*File {
	if x != nil {
		return x.Files
	}
	return nil
}

type File struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                   string       `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string       `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Url                  string       `protobuf:"bytes,3,opt,name=url,proto3" json:"url,omitempty"`
	LastModificationTime int64        `protobuf:"varint,4,opt,name=lastModificationTime,proto3" json:"lastModificationTime,omitempty"`
	Size                 int64        `protobuf:"varint,5,opt,name=size,proto3" json:"size,omitempty"` // B
	Ext                  string       `protobuf:"bytes,6,opt,name=ext,proto3" json:"ext,omitempty"`
	TransResult          *TransResult `protobuf:"bytes,7,opt,name=transResult,proto3" json:"transResult,omitempty"`
}

func (x *File) Reset() {
	*x = File{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *File) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*File) ProtoMessage() {}

func (x *File) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use File.ProtoReflect.Descriptor instead.
func (*File) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{8}
}

func (x *File) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *File) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *File) GetUrl() string {
	if x != nil {
		return x.Url
	}
	return ""
}

func (x *File) GetLastModificationTime() int64 {
	if x != nil {
		return x.LastModificationTime
	}
	return 0
}

func (x *File) GetSize() int64 {
	if x != nil {
		return x.Size
	}
	return 0
}

func (x *File) GetExt() string {
	if x != nil {
		return x.Ext
	}
	return ""
}

func (x *File) GetTransResult() *TransResult {
	if x != nil {
		return x.TransResult
	}
	return nil
}

type TransResult struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status int64  `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	Url    string `protobuf:"bytes,2,opt,name=url,proto3" json:"url,omitempty"`
}

func (x *TransResult) Reset() {
	*x = TransResult{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TransResult) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TransResult) ProtoMessage() {}

func (x *TransResult) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TransResult.ProtoReflect.Descriptor instead.
func (*TransResult) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{9}
}

func (x *TransResult) GetStatus() int64 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *TransResult) GetUrl() string {
	if x != nil {
		return x.Url
	}
	return ""
}

type SetUploadedFileRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uid      string `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	FilePath string `protobuf:"bytes,2,opt,name=filePath,proto3" json:"filePath,omitempty"`
	Size     int64  `protobuf:"varint,3,opt,name=size,proto3" json:"size,omitempty"`
}

func (x *SetUploadedFileRequest) Reset() {
	*x = SetUploadedFileRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[10]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetUploadedFileRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetUploadedFileRequest) ProtoMessage() {}

func (x *SetUploadedFileRequest) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[10]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetUploadedFileRequest.ProtoReflect.Descriptor instead.
func (*SetUploadedFileRequest) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{10}
}

func (x *SetUploadedFileRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *SetUploadedFileRequest) GetFilePath() string {
	if x != nil {
		return x.FilePath
	}
	return ""
}

func (x *SetUploadedFileRequest) GetSize() int64 {
	if x != nil {
		return x.Size
	}
	return 0
}

type SetUploadedFileReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status int64 `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SetUploadedFileReply) Reset() {
	*x = SetUploadedFileReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[11]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetUploadedFileReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetUploadedFileReply) ProtoMessage() {}

func (x *SetUploadedFileReply) ProtoReflect() protoreflect.Message {
	mi := &file_qdlxyh_fileservice_fileservice_proto_msgTypes[11]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetUploadedFileReply.ProtoReflect.Descriptor instead.
func (*SetUploadedFileReply) Descriptor() ([]byte, []int) {
	return file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP(), []int{11}
}

func (x *SetUploadedFileReply) GetStatus() int64 {
	if x != nil {
		return x.Status
	}
	return 0
}

var File_qdlxyh_fileservice_fileservice_proto protoreflect.FileDescriptor

var file_qdlxyh_fileservice_fileservice_proto_rawDesc = []byte{
	0x0a, 0x24, 0x71, 0x64, 0x6c, 0x78, 0x79, 0x68, 0x2f, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2f, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x27, 0x0a, 0x11, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68,
	0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x22,
	0x44, 0x0a, 0x0f, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x70,
	0x6c, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x19, 0x0a, 0x04, 0x66, 0x69,
	0x6c, 0x65, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x05, 0x2e, 0x66, 0x69, 0x6c, 0x65, 0x52,
	0x04, 0x66, 0x69, 0x6c, 0x65, 0x22, 0x20, 0x0a, 0x0e, 0x47, 0x65, 0x74, 0x46, 0x69, 0x6c, 0x65,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x41, 0x0a, 0x0c, 0x47, 0x65, 0x74, 0x46, 0x69,
	0x6c, 0x65, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12,
	0x19, 0x0a, 0x04, 0x66, 0x69, 0x6c, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x05, 0x2e,
	0x66, 0x69, 0x6c, 0x65, 0x52, 0x04, 0x66, 0x69, 0x6c, 0x65, 0x22, 0x28, 0x0a, 0x12, 0x47, 0x65,
	0x74, 0x41, 0x6c, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04,
	0x70, 0x61, 0x67, 0x65, 0x22, 0x79, 0x0a, 0x10, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x46, 0x69,
	0x6c, 0x65, 0x73, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x12, 0x1b, 0x0a, 0x05, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x05, 0x2e, 0x66, 0x69, 0x6c, 0x65, 0x52, 0x05, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x12, 0x12, 0x0a,
	0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x70, 0x61, 0x67,
	0x65, 0x12, 0x1c, 0x0a, 0x09, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x50, 0x61, 0x67, 0x65, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x50, 0x61, 0x67, 0x65, 0x22,
	0x25, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x4d, 0x79, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x22, 0x46, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x4d, 0x79, 0x46,
	0x69, 0x6c, 0x65, 0x73, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x12, 0x1b, 0x0a, 0x05, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x05, 0x2e, 0x66, 0x69, 0x6c, 0x65, 0x52, 0x05, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x22, 0xc6,
	0x01, 0x0a, 0x04, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x75,
	0x72, 0x6c, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x72, 0x6c, 0x12, 0x32, 0x0a,
	0x14, 0x6c, 0x61, 0x73, 0x74, 0x4d, 0x6f, 0x64, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x54, 0x69, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x14, 0x6c, 0x61, 0x73,
	0x74, 0x4d, 0x6f, 0x64, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x69, 0x6d,
	0x65, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x04, 0x73, 0x69, 0x7a, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x65, 0x78, 0x74, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x03, 0x65, 0x78, 0x74, 0x12, 0x2e, 0x0a, 0x0b, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0c, 0x2e, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x52, 0x0b, 0x74, 0x72, 0x61, 0x6e,
	0x73, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22, 0x37, 0x0a, 0x0b, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x10,
	0x0a, 0x03, 0x75, 0x72, 0x6c, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x72, 0x6c,
	0x22, 0x5a, 0x0a, 0x16, 0x53, 0x65, 0x74, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x65, 0x64, 0x46,
	0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12, 0x1a, 0x0a, 0x08,
	0x66, 0x69, 0x6c, 0x65, 0x50, 0x61, 0x74, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x66, 0x69, 0x6c, 0x65, 0x50, 0x61, 0x74, 0x68, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x22, 0x2e, 0x0a, 0x14,
	0x53, 0x65, 0x74, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x65, 0x64, 0x46, 0x69, 0x6c, 0x65, 0x52,
	0x65, 0x70, 0x6c, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x32, 0xa4, 0x02, 0x0a,
	0x0b, 0x46, 0x69, 0x6c, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x43, 0x0a, 0x0f,
	0x53, 0x65, 0x74, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x65, 0x64, 0x46, 0x69, 0x6c, 0x65, 0x12,
	0x17, 0x2e, 0x53, 0x65, 0x74, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x65, 0x64, 0x46, 0x69, 0x6c,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x53, 0x65, 0x74, 0x55, 0x70,
	0x6c, 0x6f, 0x61, 0x64, 0x65, 0x64, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22,
	0x00, 0x12, 0x34, 0x0a, 0x0a, 0x47, 0x65, 0x74, 0x4d, 0x79, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x12,
	0x12, 0x2e, 0x47, 0x65, 0x74, 0x4d, 0x79, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x10, 0x2e, 0x47, 0x65, 0x74, 0x4d, 0x79, 0x46, 0x69, 0x6c, 0x65, 0x73,
	0x52, 0x65, 0x70, 0x6c, 0x79, 0x22, 0x00, 0x12, 0x37, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x41, 0x6c,
	0x6c, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x12, 0x13, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x46,
	0x69, 0x6c, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x11, 0x2e, 0x47, 0x65,
	0x74, 0x41, 0x6c, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22, 0x00,
	0x12, 0x2b, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x46, 0x69, 0x6c, 0x65, 0x12, 0x0f, 0x2e, 0x47, 0x65,
	0x74, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x47,
	0x65, 0x74, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22, 0x00, 0x12, 0x34, 0x0a,
	0x0a, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x12, 0x12, 0x2e, 0x53, 0x65,
	0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x10, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x70, 0x6c,
	0x79, 0x22, 0x00, 0x42, 0x41, 0x5a, 0x3f, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f,
	0x6d, 0x2f, 0x68, 0x65, 0x6e, 0x72, 0x79, 0x30, 0x34, 0x37, 0x35, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x73, 0x2f, 0x71, 0x64, 0x6c, 0x78, 0x79, 0x68, 0x2f, 0x66, 0x69, 0x6c,
	0x65, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_qdlxyh_fileservice_fileservice_proto_rawDescOnce sync.Once
	file_qdlxyh_fileservice_fileservice_proto_rawDescData = file_qdlxyh_fileservice_fileservice_proto_rawDesc
)

func file_qdlxyh_fileservice_fileservice_proto_rawDescGZIP() []byte {
	file_qdlxyh_fileservice_fileservice_proto_rawDescOnce.Do(func() {
		file_qdlxyh_fileservice_fileservice_proto_rawDescData = protoimpl.X.CompressGZIP(file_qdlxyh_fileservice_fileservice_proto_rawDescData)
	})
	return file_qdlxyh_fileservice_fileservice_proto_rawDescData
}

var file_qdlxyh_fileservice_fileservice_proto_msgTypes = make([]protoimpl.MessageInfo, 12)
var file_qdlxyh_fileservice_fileservice_proto_goTypes = []interface{}{
	(*SearchFileRequest)(nil),      // 0: SearchFileRequest
	(*SearchFileReply)(nil),        // 1: SearchFileReply
	(*GetFileRequest)(nil),         // 2: GetFileRequest
	(*GetFileReply)(nil),           // 3: GetFileReply
	(*GetAllFilesRequest)(nil),     // 4: GetAllFilesRequest
	(*GetAllFilesReply)(nil),       // 5: GetAllFilesReply
	(*GetMyFilesRequest)(nil),      // 6: GetMyFilesRequest
	(*GetMyFilesReply)(nil),        // 7: GetMyFilesReply
	(*File)(nil),                   // 8: file
	(*TransResult)(nil),            // 9: transResult
	(*SetUploadedFileRequest)(nil), // 10: SetUploadedFileRequest
	(*SetUploadedFileReply)(nil),   // 11: SetUploadedFileReply
}
var file_qdlxyh_fileservice_fileservice_proto_depIdxs = []int32{
	8,  // 0: SearchFileReply.file:type_name -> file
	8,  // 1: GetFileReply.file:type_name -> file
	8,  // 2: GetAllFilesReply.files:type_name -> file
	8,  // 3: GetMyFilesReply.files:type_name -> file
	9,  // 4: file.transResult:type_name -> transResult
	10, // 5: FileService.SetUploadedFile:input_type -> SetUploadedFileRequest
	6,  // 6: FileService.GetMyFiles:input_type -> GetMyFilesRequest
	4,  // 7: FileService.GetAllFiles:input_type -> GetAllFilesRequest
	2,  // 8: FileService.GetFile:input_type -> GetFileRequest
	0,  // 9: FileService.SearchFile:input_type -> SearchFileRequest
	11, // 10: FileService.SetUploadedFile:output_type -> SetUploadedFileReply
	7,  // 11: FileService.GetMyFiles:output_type -> GetMyFilesReply
	5,  // 12: FileService.GetAllFiles:output_type -> GetAllFilesReply
	3,  // 13: FileService.GetFile:output_type -> GetFileReply
	1,  // 14: FileService.SearchFile:output_type -> SearchFileReply
	10, // [10:15] is the sub-list for method output_type
	5,  // [5:10] is the sub-list for method input_type
	5,  // [5:5] is the sub-list for extension type_name
	5,  // [5:5] is the sub-list for extension extendee
	0,  // [0:5] is the sub-list for field type_name
}

func init() { file_qdlxyh_fileservice_fileservice_proto_init() }
func file_qdlxyh_fileservice_fileservice_proto_init() {
	if File_qdlxyh_fileservice_fileservice_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchFileRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchFileReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetFileRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetFileReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllFilesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllFilesReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetMyFilesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetMyFilesReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*File); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TransResult); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[10].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetUploadedFileRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_qdlxyh_fileservice_fileservice_proto_msgTypes[11].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetUploadedFileReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_qdlxyh_fileservice_fileservice_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   12,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_qdlxyh_fileservice_fileservice_proto_goTypes,
		DependencyIndexes: file_qdlxyh_fileservice_fileservice_proto_depIdxs,
		MessageInfos:      file_qdlxyh_fileservice_fileservice_proto_msgTypes,
	}.Build()
	File_qdlxyh_fileservice_fileservice_proto = out.File
	file_qdlxyh_fileservice_fileservice_proto_rawDesc = nil
	file_qdlxyh_fileservice_fileservice_proto_goTypes = nil
	file_qdlxyh_fileservice_fileservice_proto_depIdxs = nil
}
